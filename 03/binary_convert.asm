section .text
global binary_convert
binary_convert:
	mov	rsi, 0			; set total = 0
	mov 	rdx, 0			; count bits
	mov	r8, 0b00000001		; create bit mask
step:
	movzx	ecx, byte [rdi + rdx]	; read a single byte into ecx
	inc	rdx			; count up bytes seen
	cmp	rcx, zero		; is it zero?
	je	is_zero
	cmp	rcx, one		; is it one?
	je 	is_one			
	cmp	rcx, nullchr		; is the string ended?
	je 	end
is_one:
	shl	rsi, 1			; shift bit mask to target bit
	xor 	rsi, r8			; flip 1
	jmp	step			; jump to read next 
is_zero:
	shl	rsi, 1			; shift bit mask to target bit
	jmp 	step
end:
	mov 	rax, rsi
	ret

	section	.bss
zero	equ	48
one	equ	49
nullchr equ	0
