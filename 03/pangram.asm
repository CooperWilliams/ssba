section .text
global pangram
pangram:
	mov	rsi, 0			; 26 letter flags
	mov 	rdx, 0			; count up from 0
read_one:
	movzx	ecx, byte [rdi + rdx]	; read a character
	inc	rdx			; increment char counter
	cmp	rcx, 0			; null?
	je 	end
	cmp	rcx, 65			; less than 'A'?
	jl	read_one
	cmp 	rcx, 91			; uppercase?
	jl 	to_lower		; lower-case it
	cmp 	rcx, 97			; punctuation?
	jl	read_one
	cmp 	rcx, 123		; lowercase?
	jl	flag_it			; flag it.
	jmp	read_one		; ignore others, read next
to_lower:
	add	rcx, 32			; convert to lowercase
flag_it:
	sub	rcx, 97			; zero-index for flagging
	bts	rsi, rcx		; write the flag
	jmp	read_one		; read the next character
end:
	xor	rsi, 67108863		; check if all chars seen
	cmp 	rsi, 0			; if equal, then all seen
	je	ret_true 
	mov 	rax, 0
	ret 
ret_true:
	mov rax, 1
	ret
