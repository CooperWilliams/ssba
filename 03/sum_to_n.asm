section .text
global sum_to_n
sum_to_n:
	mov	rsi, 0		; total
	mov	rdx, 0		; i
step:
	add	rsi, rdx	; total += i
	inc 	rdx
	cmp	rdx, rdi
	jle	step
end:
	mov	rax, rsi
	ret
