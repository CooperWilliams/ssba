#include <stdio.h>
#include <time.h>

#define ITERS 1000000000

int main(void)
{
    clock_t t;
    int n = 0;

    clock_t start = clock();
    for (int i = 0; i < ITERS; i++) { n = i * n; };
    clock_t timed = clock() - start;
    float secs = (float)timed / CLOCKS_PER_SEC;
    float ops = ITERS / secs;
    printf("Time taken: %d\n", timed);
    printf("Secs taken: %.9f\n", secs);
    printf("Clock speed approx %.3f GHz\n", ops / 1000000000.0);

    return 0;
}