#include <stdio.h>

int main()
{
    unsigned long a = 1L << 31;
    printf("highest unsigned int: %lu\n", a);
    printf("1 << 31 = %lu\n", a);
    printf("1 << 32 = %lu\n", a << 1);

    // printf("unintptr_t: %d\n", sizeof(a));
}
