#include <stdio.h>

int main()
{
    char letter = 'a';
    int n = 0;

    do {
        printf("%c", letter++);
        n++;
    } while (n < 256);
}