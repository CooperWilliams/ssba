import time

ITERS = 10000000

start = time.time()
for i in range(ITERS):
    pass
end = time.time() - start

print(f"Iters / sec: {ITERS // end}")
print(f"Clock speed approx: {ITERS / (end * 1000000000)} GHz")
