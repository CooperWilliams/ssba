#include <stdio.h>
#include <time.h>

#define ITERS 1000000000

int main(void)
{
    time_t seconds;
    clock_t t;
    struct tm * timeinfo;
    long long int TOTALUP = 100000000;
    int n = 0;

    // attempt 1

    time ( &seconds );
    timeinfo = localtime(&seconds);
    printf ( "Current local time and date: %s", asctime (timeinfo));
    timeinfo = localtime(&seconds);
    printf ( "Current local time and date: %s", asctime (timeinfo));

    // attempt 2

    t = clock();
    while (n < ITERS)
    {
        n += 1;
    }
    float t2 = (float)(clock() - t);
    // long long int time_taken = ((long long int)TOTALUP / (long long int)t);
    float time_taken = t2 / CLOCKS_PER_SEC;
    float ops1 = ITERS / time_taken;

    printf("n: %d \n", n);
    printf("t: %.3f \n", t2);
    printf("Measured ops / sec: %d \n", ops1);


    // attempt 3 - bradfield

    clock_t start = clock();
    for (int i = 0; i < ITERS; i++);
    float secs = (float)(clock() - start ) / CLOCKS_PER_SEC;
    float ops = ITERS / secs;
    printf("Clock speed approx %.3f GHz\n", ops / 1000000000.0);


    return 0;
}