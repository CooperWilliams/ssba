#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 1000


int get_seq(int a[])
{
    int i;
    for (i = 0; i < 10; i++) printf("%d\n", a[i]);

    return 0;
}

int get_ooo(int a[])
{
    printf("%d\n", a[998]);
    printf("%d\n", a[9]);
    printf("%d\n", a[0]);
    printf("%d\n", a[234]);
    printf("%d\n", a[23]);
    printf("%d\n", a[1]);
    printf("%d\n", a[209]);
    printf("%d\n", a[700]);
    printf("%d\n", a[100]);
    printf("%d\n", a[800]);

    return 0;
}

int i_first()
{
    int i, j;
    static int x[4000][4000];
    for (i = 0; i < 4000; i++) {
        for (j = 0; j < 4000; j++) {
            x[j][i] = i + j; 
        }
    }
}

int j_first()
{
    int i, j;
    static int x[4000][4000];
    for (j = 0; j < 4000; j++) {
        for (i = 0; i < 4000; i++) {
            x[j][i] = i + j; 
        }
    }
}

int main()
{
    // // my answer

    clock_t t, t2;

    int a[N], b[N];
    
    t = clock();
    clock_t start = clock();
    // get_seq(a);
    i_first();
    clock_t timed = clock() - start;
    float secs = (float)timed / CLOCKS_PER_SEC;
    printf("Secs taken: %.9f\n", secs);
    
    t2 = clock();
    clock_t start2 = clock();
    // get_ooo(b);
    j_first();
    clock_t timed2 = clock() - start2;
    float secs2 = (float)timed2 / CLOCKS_PER_SEC;
    printf("Secs taken: %.9f\n", secs2);
    
    // // SO answer

    
    
    return 0;
}