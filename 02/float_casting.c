//test out float casting

#include <stdio.h>

int main()
{
    unsigned int const v = 5;
    unsigned int r;

    if ( v > 1 )
    {
        float f = (float)v;
        printf("%.3f\n", f);
        unsigned int const t  = *(unsigned int *)&f >> 23;
        printf("%d\n", t);
        
    }

    return 0;
}